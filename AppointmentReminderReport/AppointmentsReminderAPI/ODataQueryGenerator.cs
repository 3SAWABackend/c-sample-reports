﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppointmentsReminderAPI
{
    /// <summary>
    /// Type safe odata query
    /// </summary>
    public class ODataQuery
    {
        public string Query { get; protected set; }

        public ODataQuery(string query)
        {
            Query = query;
        }
    }

    public class ODataQueryGenerator
    {
        public readonly string BaseUrl = "";

        public static readonly string ClientId = @"ThirdPartyService/";

        public ODataQueryGenerator(string baseUrl)
        {
            BaseUrl = baseUrl + "/" + ClientId;
        }

        /// <summary>
        /// Return OData query to get appointments with patients
        /// </summary>
        /// <param name="startTime">Value in UTC, otherwise argument exception is thrown</param>
        /// <param name="interval">interval range in minutes</param>
        /// <param name="skip"></param>
        /// <param name="top"></param>
        /// <returns></returns>
        public ODataQuery CreateGetAppointmentsWithPatients(DateTimeOffset startTime, int interval, int skip, int top)
        {
            if (startTime.Offset != TimeSpan.Zero)
                throw new ArgumentException("Value must be in UTC", nameof(startTime));
            //Appointments?$filter=DateTimeFromUTC ge 2018-05-13T09:00:00Z and DateTimeFromUTC lt 2018-05-13T09:00:00Z and Status ne WebApiModel.Enum.AppointmentStatus'Cancelled' and Status ne WebApiModel.Enum.AppointmentStatus'Blocked'&$expand=Patients($select=FullName)&$skip=500&$top=500&$count=true;
            var query = $"{BaseUrl}Appointments?$filter=DateTimeFromUTC ge {startTime.ToString("s") + "Z"} and DateTimeFromUTC lt {startTime.AddMinutes(interval).ToString("s") + "Z"} and Status ne WebApiModel.Enum.AppointmentStatus'Cancelled' and Status ne WebApiModel.Enum.AppointmentStatus'Blocked'&$select=Id,DateTimeFrom&$expand=Patient($select=FullName,MainPhoneNumber,AlternatePhoneNumber)&$skip={skip}&$top={top}";
            //we need total count only for first request
            if (skip == 0)
                query += "&$count=true";

            return new ODataQuery(query);
        }
    }
}
