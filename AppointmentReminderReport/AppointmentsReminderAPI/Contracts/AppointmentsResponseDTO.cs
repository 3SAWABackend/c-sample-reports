﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppointmentsReminderAPI.Contracts
{
    public class AppointmentsResponseDTO
    {
        [JsonProperty(PropertyName = "@odata.count")]
        public int Count { get; set; }
        [JsonProperty(PropertyName = "value")]
        public IEnumerable<AppointmentUnflattenedDTO> Appointments { get; set; }
    }
}
