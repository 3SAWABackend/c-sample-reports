﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppointmentsReminderAPI.Contracts
{
    public class AppointmentDTO
    {
        public Guid Id { get; set; }
        public DateTimeOffset DateTimeFrom { get; set; }
        public string PatientFullName { get; set; }
        public string PatientMainPhoneNumber { get; set; }
        public string PatientAlternatePhoneNumber { get; set; }
    }
}
