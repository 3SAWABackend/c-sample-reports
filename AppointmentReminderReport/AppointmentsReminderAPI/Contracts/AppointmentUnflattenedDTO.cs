﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppointmentsReminderAPI.Contracts
{
    public class AppointmentUnflattenedDTO
    {
        public Guid Id { get; set; }
        public DateTimeOffset DateTimeFrom { get; set; }
        public PatientDTO Patient { get; set; }
    }
}
