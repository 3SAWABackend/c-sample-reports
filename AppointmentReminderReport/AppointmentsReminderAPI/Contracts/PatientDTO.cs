﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppointmentsReminderAPI.Contracts
{
    public class PatientDTO
    {
        public string FullName { get; set; }
        public string MainPhoneNumber { get; set; }
        public string AlternatePhoneNumber { get; set; }
    }
}
