﻿using AppointmentsReminderAPI.Contracts;
using CSharpFunctionalExtensions;
using Microsoft.Practices.EnterpriseLibrary.TransientFaultHandling;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace AppointmentsReminderAPI
{
    public class SkedApiService
    {
        private int retryCount = 3;
        private TimeSpan minBackoffRetryPattern = TimeSpan.FromSeconds(1);
        private TimeSpan maxBackoffRetryPattern = TimeSpan.FromSeconds(4);
        private TimeSpan deltaBackoffRetryPattern = TimeSpan.FromSeconds(0.5);

        readonly HttpClient httpClient;
        readonly ODataQueryGenerator generator;
        readonly int beforeAppointmentReminderTime;
        readonly int runningInterval;
        readonly string apiKey;
        readonly int top = 500;

        public SkedApiService(int beforeAppointmentReminderTime, int runningInterval, string apiUrl, string apiKey)
        {
            this.httpClient = new HttpClient();
            this.beforeAppointmentReminderTime = beforeAppointmentReminderTime;
            this.runningInterval = runningInterval;
            this.generator = new ODataQueryGenerator(apiUrl);
            this.apiKey = apiKey;
        }

        public async Task<Result<IEnumerable<AppointmentDTO>>> GetAppointments(DateTimeOffset startTime)
        {
            var skip = 0;
            //converting to UTC
            startTime = startTime.ToUniversalTime();
            //removing Seconds component
            startTime = new DateTimeOffset(startTime.Year, startTime.Month, startTime.Day, startTime.Hour, startTime.Minute, 0, TimeSpan.Zero);
            var query = generator.CreateGetAppointmentsWithPatients(startTime, runningInterval, skip, top);
            var result = await GetObject<AppointmentsResponseDTO>(query);
            if (result.IsFailure)
                return Result.Fail<IEnumerable<AppointmentDTO>>(result.Error);
            else
            {
                var totalAppointments = result.Value.Appointments.ToList();
                var totalCount = result.Value.Count;
                if(totalCount > top)
                {
                    var count = top;
                    while(count < totalCount)
                    {
                        skip += top;
                        query = generator.CreateGetAppointmentsWithPatients(startTime, runningInterval, skip, top);
                        result = await GetObject<AppointmentsResponseDTO>(query);
                        if(result.IsFailure)
                            return Result.Fail<IEnumerable<AppointmentDTO>>(result.Error);

                        totalAppointments.AddRange(result.Value.Appointments.ToList());
                        count += result.Value.Appointments.Count();
                    }
                }

                var flattenedResults = totalAppointments.Select(x => new AppointmentDTO
                {
                    Id = x.Id,
                    DateTimeFrom = x.DateTimeFrom,
                    PatientFullName = x.Patient.FullName,
                    PatientMainPhoneNumber = x.Patient.MainPhoneNumber,
                    PatientAlternatePhoneNumber = x.Patient.AlternatePhoneNumber
                });
                return Result.Ok(flattenedResults);
            }
        }

        private async Task<Result<T>> GetObject<T>(ODataQuery query) where T : class
        {
            var retryPolicy = GetRetryPolicy();
            return await retryPolicy.ExecuteAsync(async () =>
            {
                using (var response = await httpClient.GetJson(query.Query, $"Bearer {apiKey}"))
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        var json = await response.Content.ReadAsStringAsync();
                        var returnedObject = JsonConvert.DeserializeObject<T>(json);

                        return Result.Ok(returnedObject);
                    }
                    else
                    {
                        var error = await response.Content.ReadAsStringAsync();
                        return Result.Fail<T>(error);
                    }
                }
            });
        }

        private RetryPolicy<ApiRetryPattern> GetRetryPolicy()
        {
            var retryStrategy = new ExponentialBackoff(nameof(SkedApiService), retryCount, minBackoffRetryPattern, maxBackoffRetryPattern, deltaBackoffRetryPattern, true);
            return new RetryPolicy<ApiRetryPattern>(retryStrategy);
        }

        class ApiRetryPattern : ITransientErrorDetectionStrategy
        {
            public bool IsTransient(Exception ex)
            {
                return ex.GetType() == typeof(TimeoutException);
            }
        }
    }
}
