﻿using AppointmentsReminderAPI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleApp
{
    class Program
    {
        static string keyBeforeAppointmentReminderTime = "BeforeAppointmentReminderTime";
        static string keyRunningInterval = "RunningInterval";
        static string keyApiUrl = "ApiUrl";
        static string keyApiKey = "ApiKey";

        static void Main(string[] args)
        {
            //Example call: SampleApp -BeforeAppointmentReminderTime 1440 -RunningInterval 1 -ApiUrl sked_api_url -ApiKey third_party_access_key
            //get parameters from arguments
            if (args.Count() != 8)
            {
                Console.WriteLine("Number of arguments is wrong! Arguments must be in format -BeforeAppointmentReminderTime 1440 -RunningInterval 1 -ApiUrl sked_api_url -ApiKey third_party_access_key");
                Console.WriteLine("Press ENTER to exit this sample program");
                Console.ReadLine();
                return;
            }
            var parameters = new Dictionary<string, string>();
            for (int i = 0; i < 4; i++)
            {
                parameters.Add(args[2 * i].TrimStart('-'), args[2 * i + 1]);
            }

            //use appointment reminder API
            var api = new SkedApiService(int.Parse(parameters[keyBeforeAppointmentReminderTime]), int.Parse(parameters[keyRunningInterval]), parameters[keyApiUrl], parameters[keyApiKey]);
            var taskResults = api.GetAppointments(DateTimeOffset.Now);
            taskResults.Wait();
            var results = taskResults.Result;

            //print results
            if (results.IsFailure)
                Console.WriteLine(results.Error);
            else
            {
                Console.WriteLine($"Total number of appointments: {results.Value.Count()}");
                foreach (var appointment in results.Value)
                    Console.WriteLine($"{appointment.Id} {appointment.DateTimeFrom} {appointment.PatientFullName} {appointment.PatientMainPhoneNumber} {appointment.PatientAlternatePhoneNumber}");
            }

            Console.WriteLine("Press ENTER to exit this sample program");
            Console.ReadLine();
        }
    }
}
