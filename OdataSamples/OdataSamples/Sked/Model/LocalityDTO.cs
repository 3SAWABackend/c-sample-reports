﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdataSamples.Sked.Model
{
    public class LocalityDTO : BaseDTO
    {
        public string Name { get; set; }
        public Guid RegionId { get; set; }
    }
}
