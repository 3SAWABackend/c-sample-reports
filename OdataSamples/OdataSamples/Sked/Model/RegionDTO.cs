﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdataSamples.Sked.Model
{
    public class RegionDTO : BaseDTO
    {
        public string IdExternalDatabase { get; set; }
        public string Name { get; set; }
        public string CountryCode { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public Guid CountryId { get; set; }

    }
}
