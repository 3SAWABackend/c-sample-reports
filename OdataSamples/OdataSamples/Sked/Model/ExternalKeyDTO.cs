﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdataSamples.Sked.Model
{
    public class ExternalKeyDTO
    {
        public string Key { get; set; }
        public string Origin { get; set; }
    }
}
