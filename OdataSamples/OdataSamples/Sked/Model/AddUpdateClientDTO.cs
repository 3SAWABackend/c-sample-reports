﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdataSamples.Sked.Model
{
    class AddUpdateClientDTO
    {
        public AddUpdateClientDTO()
        {
        }

        public Guid? Id { get; set; }
        public string FirstName1 { get; set; }
        public string FirstName2 { get; set; }
        public string LastName1 { get; set; }
        public string LastName2 { get; set; }
        public string FullName { get; set; }
        /// <summary>
        /// Used for patient login
        /// </summary>
        public string DocumentNumber { get; set; }
        public string DocumentType { get; set; }
      
        public string DocumentCountry { get; set; }

        public Guid? CountryId { get; set; }
        public Guid? RegionId { get; set; }


        public AddressDTO Address { get; set; }

        public DateTimeOffset? BirthDate { get; set; }

        public Gender? Gender { get; set; }
        public string MainPhoneNumber { get; set; }
        public string AlternatePhoneNumber { get; set; }

        public string Email { get; set; }
        public string Remarks { get; set; }
        public MaritalStatus? MaritalStatus { get; set; }
        public int? WeightInGrams { get; set; }
        public int? HeightInCentiMeters { get; set; }
  
    }
}
