﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace OdataSamples.Sked.Model
{
    public class CoveragePlanDTO : BaseDTO
    {

        public string Name { get; set; }
        public string Code { get; set; }
        public int Priority { get; set; }
        public bool IsPrivate { get; set; }
        public int ShortId { get; set; }
        public bool System { get; set; }
        public Guid CoverageCompanyId { get; set; }

    }
}
