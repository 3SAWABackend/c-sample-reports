﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace OdataSamples.Sked.Model
{
    public class PatientDTO : BaseDTO
    {
        public PatientDTO()
        {
            this.Address = new AddressDTO();
            this.PatientExternalKeys = new List<ExternalKeyDTO>();
        }

        [JsonProperty("@odata.etag")]
        public string Etag { get; set; }
        public string FirstName1 { get; set; }

        public string FirstName2 { get; set; }

        public string LastName1 { get; set; }

        public string LastName2 { get; set; }
        public string FullName { get; set; }
        private string documentNumber;

        /// <summary>
        /// Used for patient login
        /// </summary>

        public string DocumentNumber
        {
            get { return documentNumber; }
            set
            {
                if (value != null)
                    documentNumber = Regex.Replace(value, @"[^A-Za-z0-9]+", "");
                else
                    documentNumber = string.Empty;
            }

        }


        public string DocumentType { get; set; }
        public string DocumentCountry { get; set; }

        public bool? NoDocument { get; set; }

        public Guid? CountryId { get; set; }
        public Guid? RegionId { get; set; }

        public AddressDTO Address { get; set; }
        private DateTimeOffset? birthDate;

        public DateTimeOffset? BirthDate
        {
            get
            {
                return birthDate;
            }
            set
            {
                if (value.HasValue)
                    birthDate = new DateTimeOffset(value.Value.Date, TimeSpan.Zero);
                else
                    birthDate = null;
            }
        }

        public Gender Gender { get; set; }

        public string MainPhoneNumber { get; set; }

        public string AlternatePhoneNumber { get; set; }

        public string Email { get; set; }
        public string Remarks { get; set; }


        public MaritalStatus MaritalStatus { get; set; }

        public ICollection<ExternalKeyDTO> PatientExternalKeys { get; set; }
    }
}