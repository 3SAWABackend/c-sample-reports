﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdataSamples.Sked.Model
{
    public class ClientMapping
    {
        //used for tracking
        public Guid? Id { get; set; }
        public string DocumentType { get; set; }
        public string DocumentNumber { get; set; }
        public string DocumentCountry { get; set; }
        public string FirstName1 { get; set; }
        public string FirstName2 { get; set; }
        public string LastName1 { get; set; }
        public string LastName2 { get; set; }
        public int? CoveragePlanId { get; set; }
        public Gender Gender { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string AddressCountry { get; set; }
        public string AddressRegion { get; set; }
        public string AddressLocality { get; set; }
        public string AddressStreet { get; set; }
        public string AddressStreetNumber { get; set; }
        public string AddressFloor { get; set; }
        public string AddressApartmentNumber { get; set; }
        public string MainPhoneNumber { get; set; }
        public string AlternatePhoneNumber { get; set; }
        public string Email { get; set; }
        public string Remarks { get; set; }
    }
}
