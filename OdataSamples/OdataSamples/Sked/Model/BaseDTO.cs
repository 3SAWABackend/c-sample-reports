﻿using System;

namespace OdataSamples.Sked.Model
{
    public class BaseDTO
    {
        public bool Cancelled { get; set; }
        public Guid Id { get; set; }
        public DateTimeOffset ModifiedOn { get; set; }
        public DateTimeOffset CreatedOn { get; set; }

        public String RowVersion { get; set; }
        public string ModifiedBy { get; set; }
        public string CreatedBy { get; set; }
        //this is actually value of channel from which modification came 
        public string ModifiedByClient { get; set; }
        //this is actually value of channel that creates DTO
        public string CreatedByClient { get; set; }
    }
}
