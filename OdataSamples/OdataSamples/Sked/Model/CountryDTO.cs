﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdataSamples.Sked.Model
{
    public class CountryDTO : BaseDTO
    {

        public string CountryCode { get; set; }
        public string CountryName { get; set; }
        public string CallingPrefix { get; set; }

    }
}
