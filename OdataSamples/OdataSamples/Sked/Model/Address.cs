﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdataSamples.Sked.Model
{
    public class AddressDTO
    {
        public string Locality { get; set; }
        public string Street { get; set; }
        public string StreetNumber { get; set; }
        public string Floor { get; set; }
        public string ApartmentNumber { get; set; }
    }
}
