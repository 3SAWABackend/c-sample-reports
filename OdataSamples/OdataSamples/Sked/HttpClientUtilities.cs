﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace OdataSamples.Sked
{
    public static class HttpClientUtilities
    {
        public static async Task<HttpResponseMessage> GetJson(this HttpClient client, string url, string authorization)
        {
            using (var request = new HttpRequestMessage(HttpMethod.Get, url))
            {
                request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                if (!string.IsNullOrEmpty(authorization))
                    request.Headers.Add("Authorization", authorization);

                return await client.SendAsync(request);
            }
        }
    }
}
