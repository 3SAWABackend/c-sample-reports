﻿using CSharpFunctionalExtensions;
using Marvin.JsonPatch;
using Microsoft.Practices.EnterpriseLibrary.TransientFaultHandling;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using OdataSamples.Sked.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace AWA.Imports.Console.Services
{

    public class SkedApiService 
    {

        private int retryCount = 3;
        private TimeSpan minBackoffRetryPattern = TimeSpan.FromSeconds(60);
        private TimeSpan maxBackoffRetryPattern = TimeSpan.FromSeconds(180);
        private TimeSpan deltaBackoffRetryPattern = TimeSpan.FromSeconds(10);

        HttpClient client;

        string apikey = "";

        string baseUrl = "";

        public SkedApiService(string baseDomain, string apikey, int concurrentRequests = 100)
        {
            ServicePointManager.UseNagleAlgorithm = true;
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.CheckCertificateRevocationList = true;
            ServicePointManager.DefaultConnectionLimit = concurrentRequests;

            client = new HttpClient();
            client.Timeout = TimeSpan.FromSeconds(500);

            baseUrl = baseDomain.Last().Equals('/') ? baseDomain + "ThirdPartyService/" : baseDomain + "/ThirdPartyService/";

            //baseUrl = baseDomain + "/ThirdPartyService/";
            this.apikey = apikey;
        }

        /// <summary>
        /// Create a Client
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="SimulationMode"></param>
        /// <returns></returns>
        public async Task<Result> CreateClient(PatientDTO dto, bool SimulationMode)
        {
            var httpClientInternal = client;

            var url = baseUrl + "Patients";

            var retryStrategy = new ExponentialBackoff(nameof(SkedApiService), retryCount, minBackoffRetryPattern, maxBackoffRetryPattern, deltaBackoffRetryPattern, true);
            var retryPolicy = new RetryPolicy<SkedApiRetryStrategy>(retryStrategy);
            try
            {
                return await retryPolicy.ExecuteAsync(async () =>
                {
                    using (var request = new HttpRequestMessage(HttpMethod.Post, url))
                    {

                        var settings = new JsonSerializerSettings()
                        {
                            NullValueHandling = NullValueHandling.Ignore
                        };
                        settings.Converters.Add(new StringEnumConverter());

                        var content = new StringContent(JsonConvert.SerializeObject(dto, settings), Encoding.UTF8,
                            "application/json");


                        request.Content = content;

                        var result = await SendRequest(request, SimulationMode);

                        if (result.IsFailure) return Result.Fail(result.Error);

                        result.Value.Dispose();

                        return Result.Ok();
                    }
                });

            }
            catch (Exception e)
            {
                return Result.Fail(e.Message);
            }
        }

        public Task<Result<IEnumerable<CountryDTO>>> GetCountries()
        {
            var url = baseUrl + "Countries?$select=Id,CountryCode";

            return GetCollection<CountryDTO>(url);
        }

        public Task<Result<IEnumerable<CoveragePlanDTO>>> GetPlans()
        {
            var url = baseUrl + "CoveragePlans?$select=Id,Name,Code,Priority,IsPrivate,System,RowVersion,ShortId";

            return GetCollection<CoveragePlanDTO>(url);
        }

        public Task<Result<IEnumerable<RegionDTO>>> GetRegions()
        {
            var url = baseUrl + "Regions?$select=Id,Name,CountryId";

            return GetCollection<RegionDTO>(url);
        }

        public Task<Result<IEnumerable<LocalityDTO>>> GetLocalities()
        {
            var url = baseUrl + "Localities?$select=Id,Name,RegionId";

            return GetCollection<LocalityDTO>(url);
        }

        private async Task<Result<IEnumerable<T>>> GetCollection<T>(string url)
        {
            var retryStrategy = new ExponentialBackoff(nameof(SkedApiService), retryCount, minBackoffRetryPattern,
                maxBackoffRetryPattern, deltaBackoffRetryPattern, true);
            var retryPolicy = new RetryPolicy<SkedApiRetryStrategy>(retryStrategy);
            try
            {
                return await retryPolicy.ExecuteAsync(async () =>
                {
                    using (var request = new HttpRequestMessage(HttpMethod.Get, url))
                    {
                        var result = await SendRequest(request, false);

                        if (result.IsFailure) return Result.Fail<IEnumerable<T>>(result.Error);

                        using (var response = result.Value)
                        {
                            var json = await response.Content.ReadAsStringAsync();

                            var jObject = JObject.Parse(json);

                            return Result.Ok(jObject["value"].ToObject<IEnumerable<T>>());
                        }

                    }
                });
            }
            catch (Exception e)
            {
                return Result.Fail<IEnumerable<T>>(e.Message);
            }
        }

        private async Task<Result<HttpResponseMessage>> SendRequest(HttpRequestMessage message, bool SimulationMode)
        {
            var httpClientInternal = client;

            message.Headers.Add("Authorization", $"Bearer {apikey}");

            if (SimulationMode)
            {
                message.Content.Headers.Add("X-SimulationMode", "true");
            }


            var response = await httpClientInternal.SendAsync(message);

            if (response.IsSuccessStatusCode == false)
            {
                return Result.Fail<HttpResponseMessage>("Status code:" + response.StatusCode + " " + await response.Content.ReadAsStringAsync());
            }

            return Result.Ok(response);

        }

        /// <summary>
        /// Find patient by document
        /// </summary>
        /// <param name="documentCountry"></param>
        /// <param name="documentType"></param>
        /// <param name="documentValue"></param>
        /// <returns></returns>
        public async Task<Result<PatientDTO>> GetClientByDocument(string documentCountry, string documentType, string documentValue)
        {

            var url = baseUrl + $"Patients/SearchByDocument(DocumentCountry='{documentCountry}',DocumentNumber='{documentValue}',DocumentType='{documentType}')";

            var resultGetPatients = await GetCollection<PatientDTO>(url);

            if (resultGetPatients.IsFailure) return Result.Fail<PatientDTO>(resultGetPatients.Error);

            var patients = resultGetPatients.Value.Where(x => x.DocumentCountry == documentCountry && x.DocumentType == documentType && x.DocumentNumber.ToUpperInvariant() == documentValue.ToUpperInvariant());

            if (patients.Count() > 1) return Result.Fail<PatientDTO>("More then one patient with this document exists");
            if (patients.Count() == 0) return Result.Fail<PatientDTO>("No patient exists with this document");

            return Result.Ok(patients.First());

        }

        public async Task<Result> PatchClient(Guid id,JsonPatchDocument<PatientDTO> dto, bool SimulationMode)
        {
            var httpClientInternal = client;
            var url = baseUrl + $"Patients({id})";


            var retryStrategy = new ExponentialBackoff(nameof(SkedApiService), retryCount, minBackoffRetryPattern, maxBackoffRetryPattern, deltaBackoffRetryPattern, true);
            var retryPolicy = new RetryPolicy<SkedApiRetryStrategy>(retryStrategy);
            try
            {
                return await retryPolicy.ExecuteAsync(async () =>
                {

                    //Get If-Match value 
                    PatientDTO patient;
                    using (var request = new HttpRequestMessage(HttpMethod.Get, url))
                    {
                        var result = await SendRequest(request, false);
                        if (result.IsFailure) return Result.Fail(result.Error);
                        //get value
                        var patientJson = await result.Value.Content.ReadAsStringAsync();
                        patient = JsonConvert.DeserializeObject<PatientDTO>(patientJson);
                    }

                    using (var request = new HttpRequestMessage(new HttpMethod("PATCH"), url))
                    {

                        var settings = new JsonSerializerSettings()
                        {
                            NullValueHandling = NullValueHandling.Ignore
                        };
                        settings.Converters.Add(new StringEnumConverter());

                        var content = new StringContent(JsonConvert.SerializeObject(dto, settings), Encoding.UTF8,
                            "application/json");
                        content.Headers.Add("X-JsonPatch", "true");
                        //add if-match header
                        content.Headers.Add(HttpRequestHeader.IfMatch.ToString(),patient.Etag);

                        request.Content = content;

                        var result = await SendRequest(request, SimulationMode);

                        if (result.IsFailure) return Result.Fail(result.Error);

                        result.Value.Dispose();

                        return Result.Ok();
                    }
                });

            }
            catch (Exception e)
            {
                return Result.Fail(e.Message);
            }
        }


        private class SkedApiRetryStrategy : ITransientErrorDetectionStrategy
        {
            public bool IsTransient(Exception ex)
            {
                return true;
            }
        }
    }
}
