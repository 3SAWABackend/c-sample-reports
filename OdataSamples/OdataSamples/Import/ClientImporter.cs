﻿using AWA.Imports.Console.Services;
using CSharpFunctionalExtensions;
using Marvin.JsonPatch;
using OdataSamples.Sked.Model;
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace OdataSamples.Import
{
    /// <summary>
    /// Helper class that maps from ClientMapping to PatientDTO
    /// </summary>
    public class ClientImporter
    {
        public readonly string errorString = "Error at creating client Id:{0}. Message:{1}";

        protected readonly SkedApiService api;

        protected ConcurrentBag<int> finishedItems;

        public ClientImporter(SkedApiService api)
        {
            this.api = api;
            finishedItems = new ConcurrentBag<int>();
        }

        public int GetCountFinishedItems => finishedItems.Count;


        public async Task<Result<IEnumerable<string>>> AddClients(IEnumerable<ClientMapping> clientMappings,
         //IEnumerable<ExternalKeyMapping> externalKeys,
         IEnumerable<CoveragePlanDTO> coveragePlans,
         IEnumerable<CountryDTO> countries, IEnumerable<RegionDTO> regions, IEnumerable<LocalityDTO> localities,
         bool isSimulation)
        {
            //because of large number of clients we will group them in packages, each package with it's own UoW
            var tasks = new List<Task<Result>>();

            var processingErrors = new HashSet<string>();
            var clients = clientMappings.ToList();

            foreach (var client in clients)
            {
                tasks.Add(
                    Task.Factory.StartNew(() => AddClient(client, coveragePlans, countries, regions, localities, isSimulation)).Unwrap());
            }

            await Task.WhenAll(tasks.ToArray());

            foreach (var item in tasks)
            {
                if (item.Result.IsFailure) processingErrors.Add(item.Result.Error);
            }

            return Result.Ok(processingErrors.AsEnumerable());
        }


        public async Task<Result<IEnumerable<string>>> UpdateClients(IEnumerable<ClientMapping> clientMappings,
    //IEnumerable<ExternalKeyMapping> externalKeys,
    IEnumerable<CoveragePlanDTO> coveragePlans,
    IEnumerable<CountryDTO> countries, IEnumerable<RegionDTO> regions, IEnumerable<LocalityDTO> localities,
    bool isSimulation)
        {
            //because of large number of clients we will group them in packages, each package with it's own UoW
            var tasks = new List<Task<Result>>();

            var processingErrors = new HashSet<string>();
            var clients = clientMappings.ToList();

            foreach (var client in clients)
            {
                tasks.Add(
                    Task.Factory.StartNew(() => UpdateClient(client, coveragePlans, countries, regions, localities, isSimulation)).Unwrap());
            }

            await Task.WhenAll(tasks.ToArray());

            foreach (var item in tasks)
            {
                if (item.Result.IsFailure) processingErrors.Add(item.Result.Error);
            }

            return Result.Ok(processingErrors.AsEnumerable());
        }

        private async Task<Result> AddClient(ClientMapping clientMapping, IEnumerable<CoveragePlanDTO> coveragePlans, IEnumerable<CountryDTO> countries, IEnumerable<RegionDTO> regions, IEnumerable<LocalityDTO> localities, bool isSimulation)
        {

            var result = await MapClient(clientMapping, coveragePlans, countries, regions, localities, isSimulation)
                               .OnSuccess(dto => api.CreateClient(dto, isSimulation));
            
            if (result.IsFailure)
            {
                return Result.Fail(string.Format(errorString, clientMapping.Id, result.Error.Replace(System.Environment.NewLine, "")));
            }

            return Result.Ok();
        }

        private async Task<Result> UpdateClient(ClientMapping clientMapping, IEnumerable<CoveragePlanDTO> coveragePlans, IEnumerable<CountryDTO> countries, IEnumerable<RegionDTO> regions, IEnumerable<LocalityDTO> localities, bool isSimulation)
        {



            var result = await MapClient(clientMapping, coveragePlans, countries, regions, localities, isSimulation)
                         .OnSuccess(dto =>
                         {
                             var document = new JsonPatchDocument<PatientDTO>();

                             document.Replace(x => x.Address.ApartmentNumber, dto.Address.ApartmentNumber);
                             document.Replace(x => x.Address.Floor, dto.Address.Floor);
                             document.Replace(x => x.Address.Locality, dto.Address.Locality);
                             document.Replace(x => x.Address.Street, dto.Address.Street);
                             document.Replace(x => x.Address.StreetNumber, dto.Address.StreetNumber);
                             document.Replace(x => x.AlternatePhoneNumber, dto.AlternatePhoneNumber);
                             document.Replace(x => x.MainPhoneNumber, dto.MainPhoneNumber);
                             document.Replace(x => x.BirthDate, dto.BirthDate);
                             document.Replace(x => x.CountryId, dto.CountryId);
                             document.Replace(x => x.RegionId, dto.RegionId);
                             document.Replace(x => x.DocumentCountry, dto.DocumentCountry);
                             document.Replace(x => x.DocumentType, dto.DocumentType);
                             document.Replace(x => x.DocumentNumber, dto.DocumentNumber);
                             document.Replace(x => x.Email, dto.Email);
                             document.Replace(x => x.FirstName1, dto.FirstName1);
                             document.Replace(x => x.FirstName2, dto.FirstName2);
                             document.Replace(x => x.LastName1, dto.LastName1);
                             document.Replace(x => x.LastName2, dto.LastName2);
                             document.Replace(x => x.MaritalStatus, dto.MaritalStatus);
                             document.Replace(x => x.Gender, dto.Gender);
                             document.Replace(x => x.Remarks, dto.Remarks);

                             return Result.Ok(document);
                         })
                        .OnSuccess(document => api.PatchClient(clientMapping.Id.Value, document, isSimulation));

            if (result.IsFailure)
            {
                return Result.Fail(string.Format(errorString, clientMapping.Id, result.Error.Replace(System.Environment.NewLine, "")));
            }

            return Result.Ok();
        }

        private async Task<Result<PatientDTO>> MapClient(ClientMapping clientMapping, IEnumerable<CoveragePlanDTO> coveragePlans, IEnumerable<CountryDTO> countries, IEnumerable<RegionDTO> regions, IEnumerable<LocalityDTO> localities, bool isSimulation)
        {

            CoveragePlanDTO coveragePlan = null;
            Guid? idCountry = null;
            Guid? idRegion = null;
            string locality = null;
            //fetch 

            return await Result.Ok().OnSuccess(() =>
            {
                //assign coverageplan 
                if (clientMapping.CoveragePlanId.HasValue)
                {
                    coveragePlan = coveragePlans.SingleOrDefault(x =>
                        x.ShortId == clientMapping.CoveragePlanId.Value);
                    if (coveragePlan != null)
                    {
                        if (coveragePlan.IsPrivate) return Result.Fail(string.Format(errorString, clientMapping.Id, "Private plans can't be assigned. Leave empty"));
                        if (coveragePlan.System) return Result.Fail(string.Format(errorString, clientMapping.Id, "Plan of type system can't be assigned"));
                    }
                    else
                    {
                        return Result.Fail(string.Format(errorString, clientMapping.Id, "Assigned coverage plan can't be found"));
                    }
                }
                return Result.Ok();
            }).OnSuccess(() =>
            {

                //assign country 
                if (string.IsNullOrEmpty(clientMapping.AddressCountry) == false)
                {
                    var country = countries.SingleOrDefault(x =>
                        x.CountryCode == clientMapping.AddressCountry);
                    if (country == null)
                    {
                        return Result.Fail(string.Format(errorString, clientMapping.Id, "Assigned country can't be found"));
                    }

                    idCountry = country.Id;
                }
                return Result.Ok();
            }).OnSuccess(() =>
            {
                //assign region 
                if (string.IsNullOrEmpty(clientMapping.AddressRegion) == false)
                {
                    if (idCountry == null) return Result.Fail(string.Format(errorString, clientMapping.Id, "If a region is assigned, then also a valid country needs to be assigned"));

                    var region = regions.SingleOrDefault(x => string.Equals(x.Name, clientMapping.AddressRegion, StringComparison.InvariantCultureIgnoreCase));

                    if (region == null) return Result.Fail(string.Format(errorString, clientMapping.Id, "Assigned region can't be found"));

                    if (region != null && region.CountryId != idCountry) return Result.Fail(string.Format(errorString, clientMapping.Id, "Region does not belong to country"));


                    idRegion = region.Id;
                }

                return Result.Ok();

            }).OnSuccess(() =>
            {
                //assign locality 
                if (string.IsNullOrEmpty(clientMapping.AddressLocality) == false)
                {

                    if (idRegion == null) return Result.Fail(string.Format(errorString, clientMapping.Id, "If a locality is assigned, then also a valid region needs to be assigned"));

                    locality = clientMapping.AddressLocality;

                    var localitiesForRegion = localities.Where(x => x.RegionId == idRegion);

                    if (localitiesForRegion.Any())
                    {
                        var localityDto = localities.FirstOrDefault(x =>
                         string.Equals(x.Name, locality, StringComparison.InvariantCultureIgnoreCase));
                        if (localityDto == null)
                        {
                            return Result.Fail(string.Format(errorString, clientMapping.Id, "Locality is not valid for assigned region"));
                        }

                    }

                }

                return Result.Ok();
            }).OnSuccess(async () =>
            {
                var utcBirthDate = clientMapping.DateOfBirth.HasValue ?
                    (DateTimeOffset?)(new DateTimeOffset(
                    clientMapping.DateOfBirth.Value.Date.Year,
                    clientMapping.DateOfBirth.Value.Date.Month,
                    clientMapping.DateOfBirth.Value.Date.Day,
                    0,
                    0,
                    0,
                    TimeSpan.Zero)) : null;

                //Map to client internal
                var client = new PatientDTO
                {
                    DocumentType = clientMapping.DocumentType,
                    DocumentNumber = clientMapping.DocumentNumber,
                    DocumentCountry = clientMapping.DocumentCountry,
                    FirstName1 = clientMapping.FirstName1,
                    FirstName2 = clientMapping.FirstName2,
                    LastName1 = clientMapping.LastName1,
                    LastName2 = clientMapping.LastName2,
                    Gender = clientMapping.Gender,
                    BirthDate = utcBirthDate,
                    MainPhoneNumber = clientMapping.MainPhoneNumber,
                    AlternatePhoneNumber = clientMapping.AlternatePhoneNumber,
                    Email = clientMapping.Email,
                    Remarks = clientMapping.Remarks,
                    CountryId = idCountry,
                    RegionId = idRegion,
                    //CoveragePlans = coveragePlan != null ? new List<CoveragePlanDTO> { coveragePlan } : null,
                    Address = new AddressDTO
                    {
                        Street = clientMapping.AddressStreet,
                        StreetNumber = clientMapping.AddressStreetNumber,
                        Floor = clientMapping.AddressFloor,
                        ApartmentNumber = clientMapping.AddressApartmentNumber,
                        Locality = locality
                    },
                    //PatientExternalKeys = externalKeys.Where(x => x.EntityId == clientMapping.Id).Select(x =>
                    //     new PatientExternalKeyDTO
                    //     {
                    //         Key = x.Key,
                    //         Origin = x.Origin
                    //     }).ToList(),
                };
                return client;
            }, true).OnBoth((r) =>
            {
                return r;
            });

        }
    }
}
