﻿using AWA.Imports.Console.Services;
using CSharpFunctionalExtensions;
using OdataSamples.Import;
using OdataSamples.Sked.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdataSamples
{
    class Program
    {
        static void Main(string[] args)
        {
            Run().Wait();

        }

        //Entry point
        /*
         * User needs to input:
         * URL
         * Api key
         * Simulation mode - if the reqeust will be commited to the database
         * The number of parallel requests
         */
        static async Task Run()
        {
            System.Console.WriteLine("Base url (ex: https://proxy.qa.neoris.procloudhub.com ):");
            var stringUrl = System.Console.ReadLine();

            if (Uri.IsWellFormedUriString(stringUrl, UriKind.RelativeOrAbsolute) == false)
            {
                System.Console.WriteLine("Wrong url provided");
                return;
            }

            System.Console.WriteLine("Api key:");
            var stringApiKey = System.Console.ReadLine();

            System.Console.WriteLine("Run under simulation mode? y/n");
            var responseRunUnderSimulationMode = System.Console.ReadKey();

            var simulationMode = false;
            if (responseRunUnderSimulationMode.KeyChar == 'y') simulationMode = true;


            System.Console.WriteLine("Specify number of concurrent requests - for more then 100 please contact system support to adjust system resources - maximum 200");
            var stringInt = System.Console.ReadLine();

            if (int.TryParse(stringInt, out var concurrentRequests) == false || concurrentRequests > 200)
            {
                System.Console.WriteLine("Wrong value provided");
                System.Console.ReadKey();
                return;
            }

            System.Console.WriteLine("1. Create sample, 2. Update sample");
            var stringSample = System.Console.ReadLine();

            if (int.TryParse(stringSample, out var sample) == false)
            {
                System.Console.WriteLine("Wrong value provided");
                System.Console.ReadKey();
                return;
            }

            Result<IEnumerable<string>> result;

            if (sample == 1) result = await ImportClients(stringUrl, stringApiKey, simulationMode, concurrentRequests);
            else if (sample == 2) result = await UpdateClients(stringUrl, stringApiKey, simulationMode, concurrentRequests);
            else return;

            if (result.IsFailure)
            {
                System.Console.WriteLine(result.Error);
            }

            foreach (var item in result.Value)
            {
                System.Console.WriteLine(item);
            }




            System.Console.WriteLine("Finished");
            System.Console.ReadKey();
        }

        //this method simulates a call to the Importer call, that will map a ClientMapping object to PatientDTO
        //The importer class is designed to be as a helper in order to facilitate the import of clients
        static async Task<Result<IEnumerable<string>>> ImportClients(string proxy, string apiKey, bool simulationMode, int concurrentRequests)
        {
            IEnumerable<CoveragePlanDTO> plans = null;

            IEnumerable<CountryDTO> countries = null;
            IEnumerable<RegionDTO> regions = null;
            IEnumerable<LocalityDTO> localities = null;

            var api = new SkedApiService(proxy, apiKey, concurrentRequests);
            var importer = new ClientImporter(api);


            var mappings = new List<ClientMapping>()
            {
                new ClientMapping
                {
                    AddressApartmentNumber = "1",
                    AddressCountry = "ES",
                    AddressFloor = "1",
                    AddressLocality = "",
                    AddressRegion = "",
                    AddressStreet = "Mystreet",
                    AddressStreetNumber = "2",
                    AlternatePhoneNumber = "+34912345678", //country prefix + valid phone
                    MainPhoneNumber = "+34912345678",
                    DateOfBirth = DateTime.Now.AddYears(-5),
                    DocumentCountry = "ES",
                    DocumentType = "NationalId",
                    DocumentNumber = "109",
                    Email = "a@a.ro",
                    CoveragePlanId = null,
                    FirstName1 = "Import",
                    FirstName2 = "Dummy",
                     LastName1 = "Last",
                    LastName2 = "Name",
                    Gender = Gender.Female
                }
            };


            return await Result.Ok()
                  .OnSuccess(() => api.GetPlans(), true)
                 .OnSuccess(fetchedPlans => plans = fetchedPlans)
                 .OnSuccess(() => api.GetCountries())
                 .OnSuccess(fetchedCountries => countries = fetchedCountries)
                 .OnSuccess(() => api.GetRegions())
                 .OnSuccess(fetchedRegions => regions = fetchedRegions)
                 .OnSuccess(() => api.GetLocalities())
                 .OnSuccess(fetchedLocalities => localities = fetchedLocalities)

                .OnSuccess(() => importer.AddClients(mappings, plans, countries,
                    regions, localities, simulationMode));


        }

        //this method simulates a call to the Importer call, that will map a ClientMapping object to PatientDTO
        //The importer class is designed to be as a helper in order to facilitate the import of clients
        static async Task<Result<IEnumerable<string>>> UpdateClients(string proxy, string apiKey, bool simulationMode, int concurrentRequests)
        {
            var api = new SkedApiService(proxy, apiKey, concurrentRequests);
            var importer = new ClientImporter(api);

            //System.Console.WriteLine("Enter document country");
            //var documentCountry = System.Console.ReadLine();

            //System.Console.WriteLine("Enter document type");
            //var documentType = System.Console.ReadLine();

            //System.Console.WriteLine("Enter document number");
            //var documentNumber = System.Console.ReadLine();

            //var resultPatient = await api.GetClientByDocument(documentCountry, documentType, documentNumber);

            var resultPatient = await api.GetClientByDocument("ES", "NationalId", "109");


            if (resultPatient.IsFailure) return Result.Fail<IEnumerable<string>>(resultPatient.Error);

            System.Console.WriteLine("Enter new FirstName");
            var firstname = System.Console.ReadLine();

            System.Console.WriteLine("Enter new Street");
            var street = System.Console.ReadLine();


            IEnumerable<CoveragePlanDTO> plans = null;

            IEnumerable<CountryDTO> countries = null;
            IEnumerable<RegionDTO> regions = null;
            IEnumerable<LocalityDTO> localities = null;




            var mappings = new List<ClientMapping>()
            {
                new ClientMapping
                {
                    Id = resultPatient.Value.Id,
                    AddressApartmentNumber = "1",
                    AddressCountry = "ES",
                    AddressFloor = "1",
                    AddressLocality = "",
                    AddressRegion = "",
                    AddressStreet = street,
                    AddressStreetNumber = "2",
                    AlternatePhoneNumber = "+34912345678", //country prefix + valid phone
                    MainPhoneNumber = "+34912345678",
                    DateOfBirth = DateTime.Now.AddYears(-5),
                    DocumentCountry = "ES",
                    DocumentType = "NationalId",
                    DocumentNumber = "109",
                    Email = "a@a.ro",
                    CoveragePlanId = null,
                    FirstName1 = firstname,
                    FirstName2 = "Dummy",
                     LastName1 = "Last",
                    LastName2 = "Name",
                    Gender = Gender.Female
                }
            };


            return await Result.Ok()
                  .OnSuccess(() => api.GetPlans(), true)
                 .OnSuccess(fetchedPlans => plans = fetchedPlans)
                 .OnSuccess(() => api.GetCountries())
                 .OnSuccess(fetchedCountries => countries = fetchedCountries)
                 .OnSuccess(() => api.GetRegions())
                 .OnSuccess(fetchedRegions => regions = fetchedRegions)
                 .OnSuccess(() => api.GetLocalities())
                 .OnSuccess(fetchedLocalities => localities = fetchedLocalities)

                .OnSuccess(() => importer.UpdateClients(mappings, plans, countries,
                    regions, localities, simulationMode));


        }

    }
}
